
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
  
    entertainment_List: [{
      title: '小题库Lite',
      imglink: 'https://light-year-1258515630.cos.ap-guangzhou.myqcloud.com/%E5%B0%8F%E9%A2%98%E5%BA%93Lite/3.png',
      content: '本软件是一个刷题软件,用于有效记忆单选、多选、判断题,帮助大家通过考试。默认题库不常更新,但具有快速导入题库功能,分分钟就能添加您手中题库的并永久保存到本机,快来试试吧！',
      tag0: '学习',
      tag1: '便捷工具',
      urlLink: '/pages/introduce/introduce'
    },

    ],

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  previewImage: function (e) {
    var current = e.target.dataset.src;
    wx.previewImage({
      current: current, // 当前显示图片的http链接  
      urls: this.data.cooperation_img // 需要预览的图片http链接列表  
    })
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})